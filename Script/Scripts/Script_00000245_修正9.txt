class Game_Actor < Game_Battler
	#--------------------------------------------------------------------------
	# ● 自動戦闘用の行動候補リストを作成
	#--------------------------------------------------------------------------
	def make_action_list
		list = []
		list.push(Game_Action.new(self).set_attack)
		usable_skills.each do |skill|
			next if skill.no_auto_battle?
			next if opponents_unit.alive_members.size <= 1 && skill.for_all?
			next unless skill.stypes.any? {|type| added_skill_types.include?(type) }
			if $game_system.conf[:bt_stype]
				next if skill.stypes.any? {|type| skill_type_disabled?(type) }
			end
			list.push(Game_Action.new(self).set_skill(skill.id))
		end
		list
	end
	#--------------------------------------------------------------------------
	# ● 自動戦闘時の戦闘行動を作成
	#--------------------------------------------------------------------------
	def make_auto_battle_actions
		@actions.size.times do |i|
			@actions[i] = make_action_list.sample
		end
	end
end

class Game_Temp
	attr_accessor :floor_damage
end

class Game_Actor < Game_Battler
	#--------------------------------------------------------------------------
	# ● 床ダメージの処理
	#--------------------------------------------------------------------------
	def execute_floor_damage
		$game_temp.floor_damage ||= (basic_floor_damage * $game_party.floor_damage_rate).to_i
		self.hp -= [$game_temp.floor_damage, max_floor_damage].min
		perform_map_damage_effect if $game_temp.floor_damage > 0
	end
end

class Game_Party < Game_Unit
  #--------------------------------------------------------------------------
  # ● プレイヤーが 1 歩動いたときの処理
  #--------------------------------------------------------------------------
  def on_player_walk
		$game_temp.floor_damage = nil
    members.each {|actor| actor.on_player_walk}
  end
	
end


#アビリティの調整
#==============================================================================
# ■ Game_Actor
#==============================================================================
class Game_Actor < Game_Battler
	def refresh_ability(stype_id)
		while ap(stype_id) > max_ap(stype_id)
			equip_abilities[stype_id].pop
		end
	end
	def refresh_ability_all
		equip_abilities.each_key{|key| refresh_ability(key)}
	end
	
end

class RPG::Skill < RPG::UsableItem
	def ability?
		NWConst::Ability::ABILITY_SKILL_TYPE.include?(stype_id)
	end
end

#全処分
class Game_Interpreter
	def sell_item(item)
		$game_party.lose_item(item,1)
		$game_party.gain_gold(item.price/2)
	end
	
	def sell_enchant_weapons
		sell_items = $game_party.enchant_weapons.reject{|item|$game_party.favorite_item?(item)}
		sell_items.each{|item|
			sell_item(item)
		}
	end
	
	def sell_enchant_armors
		sell_items = $game_party.enchant_armors.reject{|item|$game_party.favorite_item?(item)}
		sell_items.each{|item|
			sell_item(item)
		}
	end
end

class RPG::BaseItem
	def enchant?
		false
	end
end

class RPG::Class < RPG::BaseItem
	def enchant?
		@id >= 1500
	end
end

module HIMA
	
end


module HIMA::Enchant
	def self.display_color(id)
		case id
		when 0
			0
		when 1
			23
		when 2
			6
		when 3
			2
		when 4
			11
		end
	end
end


