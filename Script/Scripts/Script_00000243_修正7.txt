#==============================================================================
# ■ Game_Actor
#==============================================================================
class Game_Actor < Game_Battler
  #--------------------------------------------------------------------------
  # ○ 通常能力値の加算値取得
  #--------------------------------------------------------------------------
  def param_plus(param_id)
    base = [@param_plus[param_id], max_param_plus(param_id)].min
    equips.compact.inject(base) do |r, item|
      value = item.params[param_id]
      value *= item_mastery_rate(item) if value > 0
      r += value # to_iは不要
    end
  end
end
class RPG::BaseItem
  alias hima_s7_nw_note_analyze nw_note_analyze
  def nw_note_analyze
    hima_s7_nw_note_analyze
    change_features
  end

  def change_features
    @features_override = default_features
  end

  def default_features
    @features + @add_features
  end
end
class RPG::Enemy < RPG::BaseItem
  def change_features
    hit_features, other_features = default_features.partition do |feature|
      feature.code == FEATURE_XPARAM && feature.data_id == 0
    end
    hit_value = hit_features.empty? ? 0.8 : hit_features.map(&:value).inject(&:+)
    hit_feature = RPG::BaseItem::Feature.new(
      FEATURE_XPARAM_EX, 0, hit_value
    )
    @features_override = [hit_feature] + other_features
  end
end
#==============================================================================
# ■ RPG::Weapon
#==============================================================================
class RPG::Weapon < RPG::EquipItem
  #--------------------------------------------------------------------------
  # ● メモ欄解析処理
  #--------------------------------------------------------------------------
  def nw_note_analyze
    nw_kure_weapon_note_analyze
    note.each_line do |line|
      if NWRegexp::Weapon::NOT_DUAL_WIELD.match(line)
        @data_ex[:not_dual_wield?] = true
      end
    end
  end

  def change_features
    hit_features, other_features = default_features.partition do |feature|
      feature.code == FEATURE_XPARAM && feature.data_id == 0
    end
    hit_value = hit_features.empty? ? 0.8 : hit_features.map(&:value).inject(&:+)
    hit_feature = RPG::BaseItem::Feature.new(
      FEATURE_XPARAM_EX, 0, hit_value
    )
    @features_override = [hit_feature] + other_features
  end
end

class Game_BattlerBase
  #--------------------------------------------------------------------------
  # ○ 追加能力値の取得
  #--------------------------------------------------------------------------
  def xparam(xparam_id)
    case xparam_id
    when 0
      hit_base(xparam_id) + features_sum(FEATURE_XPARAM, xparam_id)
    when 3
      features_sum(FEATURE_XPARAM, xparam_id)
    else
      features_xparam_rate(FEATURE_XPARAM, xparam_id)
    end
  end

  def hit_base(xparam_id)
    values = features_with_id(FEATURE_XPARAM_EX, xparam_id).map(&:value)
    return 0.8 if values.empty?

    values.inject(&:+) / values.size * 1.0
  end
end

